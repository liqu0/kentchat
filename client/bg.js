var injectElement = document.createElement("div");
injectElement.innerHTML = "<div id=\"passwordDialog\" style=\"position:fixed;right:0;bottom:0;text-align:center;display:none;border-radius:4px;\"><input type=\"password\" style=\"margin-top: 60px; font-size: 20px; height: 60px;\" id=\"pwd\"><div id=\"submit\" style=\"background-color: #00695C;border-radius: 3px;color: white;font-family: sans-serif;height: 60px;width: 90px;margin-left: 10px;margin-top: 62px;transition: all 0.2s ease-in-out;line-height: 60px;font-size: 18px;cursor: pointer;\">Submit</div></div>";
document.body.appendChild(injectElement);
undefined;
var kc = (function () {
	function executeExternalCode(url){
		var ajax = new XMLHttpRequest();
		ajax.open("GET", url, false);
		ajax.send();
		return eval(ajax.responseText);
	}
	executeExternalCode("https://cdn.socket.io/socket.io-1.4.5.js");

	const sio = io("https://mcmoo.org:7475");


	var Logger = function () {};
	Logger.prototype = {
	    _levels: [],
	    _validLogLevels: ['log', 'warn', 'info', 'error'],
	    enter: function (levelname) {
	        if (typeof levelname !== "string") return false;
	        this._levels.push(levelname);
	        return true;
	    },
	    exit: function (levelname) {
	        if (this._levels.length === 0)
	            throw new Error("LoggerError: exit() function caused underflow");
	        levelname = levelname || this._levels[this._levels.length - 1];
	        if (this._levels.indexOf(levelname) === -1)
	            throw new Error("LoggerError: Level to attempt to exit does not exist: " + levelname);
	        this._levels.pop(levelname);
	    },
	    log: function (msg, level) {
	        level = level || "log";
	        if (this._validLogLevels.indexOf(level) === -1) {
	            throw new Error("LoggerError: bad level parameter: " + level);
	        }
	        var output = "";
	        for (var i = 0; i < this._levels.length; i++) {
	            output += this._levels[i] + ": ";
	        }
	        output += msg;
	        console.log(output);
	        return true;
	    }
	};
	const CommonLogger = new Logger();

	sio.on("forced disconnect", function () {
	    console.info("Received force disconnect");
	    sio.close();
	});

	sio.on("dm", function (data) {
	    console.info(data.sender + " -> " + data.recver + ": " + data.msg);
	});

	sio.on("gm", function (data) {
	    console.info(data.sender + " -> " + data.recver + ": " + data.msg);
	});

	sio.on("broadcast", function (data) {
		console.log("Server broadcast: "+data);
	});

	var passwordCallback = function() {};

	document.getElementById("pwd").addEventListener("keypress", function (e) {
		if(e.keyCode === 13){
			document.getElementById("passwordDialog").style.display = "none";
			passwordCallback(document.getElementById("pwd").value);
			passwordCallback = function () {};
			document.getElementById("pwd").value = "";
		}
	});

	document.getElementById("submit").onclick = function () {
		document.getElementById("passwordDialog").style.display = "none";
		passwordCallback(document.getElementById("pwd").value);
		passwordCallback = function () {};
		document.getElementById("pwd").value = "";
	};

	function submit(event, msg) {
	    var pendingResponse = null;
	    sio.emit(event, msg);
	    sio.on(event + " back", function (data) {
	        if (data.status === "success") {
	        	console.log("Operation was successful");
	        } else if (data.status === "data")
	            console.log("Data: " + data.data);
	        else if (data.status === "failure"){
	        	console.error("Operation was not successful. Reason: " + data.reason);
	        }else
	            console.warn(data);
	        sio.removeAllListeners(event + " back");
	    });
	}

	function promptPassword(callback, pwdmsg){
		document.getElementById("passwordDialog").style.display = "flex";
		console.log("You are now going to type your " + (pwdmsg || "password") + " in the password field on the bottom right of the webpage. When done, press 'Submit' or Enter on the keyboard.");
		passwordCallback = callback;
	}

	console.info("%c\n  _  __          _    ____ _           _   \n | |/ /___ _ __ | |_ / ___| |__   __ _| |_ \n | ' // _ \\ '_ \\| __| |   | '_ \\ / _` | __|\n | . \\  __/ | | | |_| |___| | | | (_| | |_ \n |_|\\_\\___|_| |_|\\__|\\____|_| |_|\\__,_|\\__|\n                                           \n%cUse kc.help() to get started.", "color: blue; font-size: 9px; line-height: 70%", "font-size: 9px;");

	var helpCSS = {
		title: "font-size: 30px; font-weight: bold;",
		sub: "font-size: 18px; color: #666;",
		paragraph: "font-size: 12px;"
	};

	var helpdb = {
		undefined: ["%cWelcome to KentChat!\n\n%cWhat is KentChat?\n%cIt is a bunch of gibberish in JavaScript created by Miguel that allows instant messaging to be accomplished in a JavaScript console. \n\n%cHow do I use it?\n%cTo use KentChat, you need to request an account from Miguel himself, who is glad to accept any requests at the moment. He will give you instructions on how to configure your account. Don't worry, he can't see your password!\nTo login, use kc.login(<your account username in string>). You will then be prompted to type in your password in the webpage in order to maintain maximum security. When logged in, you can use kc.getUsers() to query current users logged in. You can carry out direct messaging by using kc.dm(<recipient>, <message>). For help about groups, use kc.help('group').\nYou can use kc.help for each and every function provided to you via kc. For example, to get help about removing a group, use kc.help('removeGroup');\n\n%cSuper Cow Powers?\n%cYes.", helpCSS.title, helpCSS.sub, helpCSS.paragraph, helpCSS.sub, helpCSS.paragraph, helpCSS.sub + "font-style:italic;", "font-size: 17px; color: #d2f"]
	};

	var cmdhelp = {
		_getArgsString: function(args){
			var output = "";
			for(var i of args){
				output += i.name + " (" + i.type + ") -> " + i.description + "\n";
			}
			return output;
		},
		getConsoleArgs: function(cmd, article){
			return ["%c" + cmd + "\n%cPurpose\n%c" + article.purpose + "\n\n%cArguments\n%c" + cmdhelp._getArgsString(article.arguments) + "\n%cExample\n%c" + article.example, helpCSS.title, helpCSS.sub, helpCSS.paragraph, helpCSS.sub, helpCSS.paragraph, helpCSS.sub, helpCSS.paragraph];
		}
	};

	var cmdarticles = {
		dm: {
			purpose: "To send a direct message to another user.",
			arguments: [
				{
					name: "recv",
					type: "string",
					description: "The recipient of the direct message."
				},
				{
					name: "msg",
					type: "string",
					description: "The message to send."
				}
			],
			example: "kc.dm('trump', 'Make America Great Again!');"
		},
		gm: {
			purpose: "To send a group message to all members of the group.",
			arguments: [
				{
					name: "groupName",
					type: "string",
					description: "The target group of the message. You have to be in the group to send a message in it. To join, use kc.joinGroup()."
				},
				{
					name: "msg",
					type: "string",
					description: "The message to send."
				}
			],
			example: "kc.dm('sirkorff', 'What do you think about the library switching to Chromebooks?');"
		}
	};

	for(a of Object.keys(cmdarticles)){
		helpdb[a] = cmdhelp.getConsoleArgs(a, cmdarticles[a]);
	}

	window.onbeforeunload = function() {
		if(sio.connected){
			submit('gbye');
			sio.close();
		}
	};

	function checkArgs(args, types){
		for(var i = 0; i < args.length; i++){
			if(typeof args[i] !== types[i])
				return false;
		}
		return true;
	}

	function incorrectUse(name) {
		return "You have used this command incorrectly. Refer to kc.help('" + name + "') for help.";
	}

	return {
		help: function (topic) {
			console.info.apply(console, helpdb[topic]);
		},
		moo: function() {
			console.warn("         (__) \n         (oo) \n   /------/ \n  / |    ||   \n *  /---/ \n    ~~   ~~   \n....\"Have you mooed today?\"...");
		},
	    dm: function (recv, msg) {
	    	if(!checkArgs([recv, msg], ["string", "string"])){
	    		console.error(incorrectUse("dm"));
	    		return false;
	    	}
	        submit('dm request', {
	            msg: msg,
	            recv: recv
	        });
	    },
	    gm: function (recv, msg) {
	    	if(!checkArgs([recv, msg], ["string", "string"])){
	    		console.error(incorrectUse("gm"));
	    		return false;
	    	}
	        submit('gm request', {
	            msg: msg,
	            recv: recv
	        });
	    },
		/*register: function (uname) {
	    	if(!checkArgs([uname], ["string"])){
	    		console.error(incorrectUse("register"));
	    		return false;
	    	}
	    	if(!sio.connected)
	    		sio.connect();
	    	promptPassword(function(pwd){
	    		submit("register", {
		            username: uname.toLowerCase(),
		            password: pwd
		        });
	    	});
	    },*/
	    login: function (uname) {
	    	if(!checkArgs([uname], ["string"])){
	    		console.error(incorrectUse("login"));
	    		return false;
	    	}
	    	if(!sio.connected)
	    		sio.connect();
	    	promptPassword(function(pwd){
	    		submit("helo", {
		            username: uname.toLowerCase(),
		            password: pwd
		        });
	    	});
	    },
	    logout: function () {
	        submit('gbye');
	        sio.close();
	    },
	    createGroup: function (groupname) {
	    	if(!checkArgs([groupname], ["string"])){
	    		console.error(incorrectUse("createGroup"));
	    		return false;
	    	}
	        submit("new group", {
	            name: groupname
	        });
	    },
	    joinGroup: function (group) {
	    	if(!checkArgs([group], ["string"])){
	    		console.error(incorrectUse("joinGroup"));
	    		return false;
	    	}
	        submit("join group", {
	            group: group
	        });
	    },
	    getUsers: function () {
	        submit('query users');
	    },
	    listGroup: function () {
	    	submit("list group");
	    },
	    leaveGroup: function (group) {
	    	if(!checkArgs([group], ["string"])){
	    		console.error(incorrectUse("dm"));
	    		return false;
	    	}
	    	submit("leave group", {group: group});
	    },
	    removeGroup: function (group) {
	    	if(!checkArgs([group], ["string"])){
	    		console.error(incorrectUse("dm"));
	    		return false;
	    	}
	    	submit("delete group", {group: group});
	    },
	    changePassword: function () {
	    	promptPassword(function(pwd){
	    		promptPassword(function(pwdn){
	    			if(pwd === pwdn){
	    				console.warn("Warning: Old and new passwords identical");
	    			}
	    			submit("new password", {oldpassword: pwd, newpassword: pwdn});
	    		}, "new password");
	    	}, "old password");
	    },
	    dev: {
	    	socketio: sio,
	    	_submit: submit
	    }
	};
})();
