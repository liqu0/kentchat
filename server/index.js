const fs = require("fs");
const https = require("https");
const _webserver = https.createServer({
	key: fs.readFileSync("./privkey.pem"),
	cert: fs.readFileSync("./cert.pem"),
}, (req, res) => {});
const io = require("socket.io")(_webserver);
const sha1 = require("sha1");

var connectedUsers = {};
var groups = {};

var Logger = function () {
	return {
	    _levels: [],
	    _validLogLevels: ['log', 'warn', 'info', 'error'],
	    enter: function (levelname) {
	        if (typeof levelname !== "string") return false;
	        this._levels.push(levelname);
	        return true;
	    },
	    exit: function (levelname) {
	        if (this._levels.length === 0)
	            throw new Error("LoggerError: exit() function caused underflow");
	        levelname = levelname || this._levels[this._levels.length - 1];
	        if (this._levels.indexOf(levelname) === -1)
	            throw new Error("LoggerError: Level to attempt to exit does not exist: " + levelname);
	        this._levels.pop(levelname);
	    },
	    log: function (msg, level) {
	        level = level || "log";
	        if (this._validLogLevels.indexOf(level) === -1) {
	            throw new Error("LoggerError: bad level parameter: " + level);
	        }
	        var output = "";
	        for (var i = 0; i < this._levels.length; i++) {
	            output += this._levels[i] + ": ";
	        }
	        output += msg;
	        console.log(output);
	        return true;
	    }
	};
};
const CommonLogger = new Logger();
const ChatLog = new Logger();
ChatLog.enter("ChatLogger");

CommonLogger.enter("KentChat");

function getUsername(socket) {
    for (var i of Object.keys(connectedUsers)) {
        if (connectedUsers[i] === socket.id)
            return i;
    }
}

function containsKeys(obj, keys) {
    for (var m of[].slice.call(keys)) {
        if (Object.keys(obj).indexOf(m) === -1) {
            return false;
        }
    }
    return true;
}

function insertEventHandler(obj, sock) {
    sock.on(obj.event, function (data) {
        if (!getUsername(sock) && obj.event !== "helo") {
            CommonLogger.log("Invalid operation from socket ID (no HELO): " + sock.id);
            sock.emit("forced disconnect");
        } else {
            CommonLogger.enter("Handler['" + obj.event + "']");
            var response = obj.handler(data, sock);
            if (response)
                sock.emit(obj.event + " back", response);
            CommonLogger.exit();
        }
    });
}

const fsm = {
	addEntry: function (msgstr) {
		fs.appendFile('/projects/kentchat/log.log', msgstr+"\n", function (err) {
			if(err)
				console.error("Filesystem error! " + err);
		});
	},
	formatMsg: function (msg, sender, groupMsg) {
		var d = new Date();
		return "["+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds()+"] "+(groupMsg?"(GROUP)":"")+sender+" -> "+msg.recv+": "+msg.msg;
	},
	push: function (msg, sender, groupMsg) {
		fsm.addEntry(fsm.formatMsg(msg, sender, groupMsg));
	},
	outputAuth: function() {
		fs.writeFile('/projects/kentchat/auth.secret', JSON.stringify(fsm.auth), 'utf8');
	}
};
fs.readFile('/projects/kentchat/auth.secret', 'utf8', function (err, data) {
	if(err) throw err;
	fsm.auth = JSON.parse(data);
});

const group = {
    create: function (name, members) {
		if (Object.keys(groups).indexOf(name) !== -1) {
            CommonLogger.log("Attempt to create group with existent name: " + data.name);
            return {
                status: "failure",
                reason: "Name already exists"
            };
        }
        // members need to be usernames
        groups[name] = {};
        groups[name].members = members || [];
        CommonLogger.log("New group created with name " + name);
        return {
            status: "success"
        };
    },
    list: function () {
		return {
			status: "data",
			data: Object.keys(groups)
		};
    },
    join: function (source, group) {
		// source needs to be a username
		if(Object.keys(groups).indexOf(group) !== -1){
			if(groups[group].members.indexOf(source) === -1){
				groups[group].members.push(source);
				CommonLogger.log(source + " joined group " + group);
				return {
					status: "success"
				};
			} else {
				CommonLogger.log("Null attempt to join group " + group + " from " + source + ", already in group");
				return {
					status: "failure",
					reason: "Already in group"
				};
			}
		} else {
			CommonLogger.log("Attempt to join nonexistent group: " + group);
			return {
				status: "failure",
				reason: "Group nonexistent"
			};
		}
    },
    leave: function (source, group) {
		if(Object.keys(groups).indexOf(group) !== -1){
			if(groups[group].members.indexOf(source) !== -1){
				groups[group].members.pop(source);
				CommonLogger.log(source + " left group " + group);
				return {
					status: "success"
				};
			} else {
				CommonLogger.log("Null attempt to leave group " + group + " from " + source + ", already left");
				return {
					status: "failure",
					reason: "Already left group"
				};
			}
		} else {
			CommonLogger.log("Attempt to leave nonexistent group");
			return {
				status: "failure",
				reason: "Group nonexistent"
			};
		}
    },
    remove: function (source, group) {
    	if(Object.keys(groups).indexOf(group) !== -1){
			if(groups[group].members.indexOf(source) !== -1){
				CommonLogger.log(source + " is deleting group " + group);
				for(var o of groups[group].members){
					io.to(connectedUsers[o]).emit("broadcast", source + " is deleting group " + group + ", so you are ejected from it.");
				}
				fsm.addEntry(group + " (GROUP) was deleted by user " + source);
				return {
					status: "success"
				};
			} else {
				CommonLogger.log("Null attempt to delete group " + group + " from " + source + ", not in group");
				return {
					status: "failure",
					reason: "Not in group"
				};
			}
		} else {
			CommonLogger.log("Attempt to delete nonexistent group");
			return {
				status: "failure",
				reason: "Group nonexistent"
			};
		}
    }
};

io.on("connection", function (socket) {
    CommonLogger.log("New RAW connection");
	socket.on("register", function (data) {
		if (!data || !data.username || !data.password) {
			socket.emit("register back", {status: "failure", reason: "unable to register: missing username/password"});
		} else if (fsm.auth[data.username]) {
			socket.emit("register back", {status: "failure", reason: "unable to register: username occupied"});
		} else {
			fsm.auth[data.username] = sha1(data.password);
			socket.emit("register back", {status: "success"});
			CommonLogger.log("New client registered: " + data.username);
		}
	});
    socket.on("helo", function (data) {
        if (!data || !data.username || !data.password) {
            CommonLogger.log("Invalid HELO encountered from ID " + socket.id);
            socket.emit("helo back", {
                status: "failure",
                reason: "Missing username/password"
            });
        } else if (getUsername(socket)) {
        	CommonLogger.log("Attempt to HELO while logged in: " + getUsername(socket));
            socket.emit("helo back", {
                status: "failure",
                reason: "Already logged in"
            });
        } else if (sha1(data.password) !== fsm.auth[data.username]) {
        	CommonLogger.log("Failed authentication from " + data.username);
        	socket.emit("helo back", {
                status: "failure",
                reason: "Failed to authenticate"
            });
        } else {
            connectedUsers[data.username] = socket.id;
            socket.emit("helo back", {
                status: "success"
            });
            CommonLogger.log("New client connected: " + data.username);
            CommonLogger.log("Current hierarchy:");
            CommonLogger.log(JSON.stringify(connectedUsers));
        }
    });
    insertEventHandler({
        event: "list group",
        handler: function (data, socket) {
            return group.list();
        }
    }, socket);
    insertEventHandler({
        event: "new group",
        handler: function (data, socket) {
            if (!data || !data.name) {
                CommonLogger.log("Invalid NEW GROUP encountered");
                return {
                    status: "failure",
                    reason: "No name specified"
                };
            }
            return group.create(data.name, data.members);
        }
    }, socket);
    insertEventHandler({
        event: "gbye",
        handler: function (data, socket) {
            var uname = getUsername(socket);
            if (uname) {
                delete connectedUsers[uname];
                CommonLogger.log("Client GBYE: " + uname);
                CommonLogger.log("New hierarchy: " + JSON.stringify(connectedUsers));
            }
            return {
                status: "success"
            };
        }
    }, socket);
    insertEventHandler({
        event: "dm request",
        handler: function (data, socket) {
            // expected keys: msg, recv
            if (containsKeys(data, ['msg', 'recv']) && typeof data.msg === "string" && typeof data.recv === "string") {
                if (Object.keys(connectedUsers).indexOf(data.recv) === -1) {
                    return {
                        status: "failure",
                        reason: "Unknown user"
                    };
                }
                io.to(connectedUsers[data.recv]).emit("dm", {
                    msg: data.msg,
                    sender: getUsername(socket),
                    recver: data.recv
                });
                io.to(socket.id).emit("dm", {
                    msg: data.msg,
                    sender: getUsername(socket),
                    recver: data.recv
                });
                fsm.push(data, getUsername(socket), false);
                ChatLog.log(fsm.formatMsg(data, getUsername(socket), false));
                return {
                    status: "success"
                };
            } else {
                return {
                    status: "failure",
                    reason: "Invalid data"
                };
            }
        }
    }, socket);
    insertEventHandler({
        event: "query users",
        handler: function (data, socket) {
            return {
                status: "data",
                data: Object.keys(connectedUsers)
            };
        }
    }, socket);
    insertEventHandler({
        event: "gm request",
        handler: function (data, socket) {
            // expected keys: msg, recv
            if (containsKeys(data, ['msg', 'recv']) && typeof data.msg === "string" && typeof data.recv === "string") {
                if (Object.keys(groups).indexOf(data.recv) === -1) {
                    return {
                        status: "failure",
                        reason: "Unknown group"
                    };
                }
                if (groups[data.recv].members.indexOf(getUsername(socket)) === -1) {
                    return {
                        status: "failure",
                        reason: "Not in group"
                    };
                }
                for (var i of groups[data.recv].members) {
                    io.to(connectedUsers[i]).emit("gm", {
                        msg: data.msg,
                        sender: getUsername(socket),
                        recver: data.recv
                    });
                }
                fsm.push(data, getUsername(socket), true);
                ChatLog.log(fsm.formatMsg(data, getUsername(socket), true));
                return {
                    status: "success"
                };
            } else {
                return {
                    status: "failure",
                    reason: "Invalid data"
                };
            }
        }
    }, socket);
    insertEventHandler({
        event: "join group",
        handler: function (data, socket) {
        	if(!data || !data.group)
        		return {
        			status: "failure",
        			reason: "malformed data"
        		};
            return group.join(getUsername(socket), data.group);
        }
    }, socket);
    insertEventHandler({
    	event: "leave group",
    	handler: function (data, socket) {
    		if(!data || !data.group)
    			return {
    				status: "failure",
    				reason: "malformed data"
    			};
    		return group.leave(getUsername(socket), data.group);
    	}
    }, socket);
    insertEventHandler({
    	event: "delete group",
    	handler: function (data, socket) {
    		if(!data || !data.group)
    			return {
    				status: "failure",
    				reason: "malformed data"
    			};
    		return group.remove(getUsername(socket), data.group);
    	}
    }, socket);
    insertEventHandler({
    	event: "new password",
    	handler: function (data, socket) {
    		if(!data || !data.oldpassword || !data.newpassword)
    			return {
    				status: "failure",
    				reason: "malformed data"
    			};
    		if(sha1(data.oldpassword) !== fsm.auth[getUsername(socket)])
    			return {
    				status: "failure",
    				reason: "Old password incorrect"
    			};
    		fsm.auth[getUsername(socket)] = sha1(data.newpassword);
    		fsm.outputAuth();
    		return {
    			status: "success"
    		};
    	}
    }, socket);
    socket.on("disconnect", function () {
        user = getUsername(socket);
        if (user) {
        	for(var i of Object.keys(groups)){
        		if (group.leave(user, i).status === "success"){
        			CommonLogger.log("Disconnection of user " + user + " caused ejection from group " + i);
        		}
        	}
            delete connectedUsers[user];
            CommonLogger.log("Client disconnected: " + user);
            CommonLogger.log("New hierarchy: " + JSON.stringify(connectedUsers));
        }
    });
});
_webserver.listen(7475);
